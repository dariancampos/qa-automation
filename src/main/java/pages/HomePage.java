package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends PageObject {
    @FindBy(xpath= "//*[@id='email']")
    public WebElement findLoginUsername;
    @FindBy(xpath= "//*[@id='pass']")
    public WebElement findLoginPass;
    @FindBy(xpath= "//*[@id='u_0_3']")
    public WebElement findLoginButton;

    public HomePage(WebDriver driver) {
        super(driver);
        open();
    }

    public void loginFace(){
        findLoginUsername.click();
        findLoginUsername.clear();
        findLoginUsername.sendKeys("darian.campos");
        findLoginPass.click();
        findLoginPass.clear();
        findLoginPass.sendKeys("Ubuntu1234$");
        findLoginButton.click();
    }
}